#include "unity.h" //---PASO 1
#include "gpio.h" // la función a evaluar
#include "phase.h"

void setUp(void) //----PASO 1
    {  // las cosas que son comunes en los test se pueden colocar aqui
        // como variables y funciones
        //primero se ejecuta este setUp antes de cada test

    }
void tearDown(void)
    {

    }
void test_gpioDefaultAngle(void)   /// PRIMER TEST , El requetimiento indica que el angulo por defecto
                                    // es decir el de inicio de sistema sea el de 0xFFF7
    {
       uint16_t gpioValue = 0xFFFF;

        gpio_default(&gpioValue);

        TEST_ASSERT_EQUAL_HEX16(0xFFF7,gpioValue);


    }



void test_txRealPhase(void)    // TERCER TEST , el requerimiento indica que la phase calculada entre la deseada y la salida
                               // debe ser la misma que se envia para la transmision por ethernet
    {
        uint16_t phase= 0;  // variable que retornará con valor de fase calculada
        uint16_t valorADC;
        uint16_t txphase=0; // valor transmitido via ethernet

        valorADC=readADC();
        phase=calculatePhase(valorADC);
        txphase=txPhase(phase);
        TEST_ASSERT_EQUAL_HEX16(phase,txphase);
    }

void test_okCircuits(void)   //compara los GPIOS transmitidos con los GPIOS recibidos para indicar si existe error circuital
                               //o algun inconveniente con los Relés
    {
        uint16_t gpioValue = 1;

        bool checkGpio=false;
        uint16_t angle=359; // angulo de prueba

       gpioValue = gpio_setAngle(angle);

       checkGpio=gpio_compareInOut(angle);

        TEST_ASSERT_EQUAL_HEX16(true,checkGpio);
    }

void test_failCircuits(void)   //compara los GPIOS transmitidos con los GPIOS recibidos para indicar si existe error circuital
                               //o algun inconveniente con los Relés
    {
        uint16_t gpioValue = 1;

        bool checkGpio=true;
        uint16_t angle=357; // angulo de prueba

       gpioValue = gpio_setAngle(angle);

       checkGpio=gpio_compareInOut(angle);

        TEST_ASSERT_EQUAL_HEX16(false,checkGpio);
    }
void test_failAngle(void) // el requerimiento indica que se debe  informar si existe
                          // una diferencia entre el angulo deseado y leido en ADC > 5ª
{
        uint16_t phase= 0;  // variable que retornará con valor de fase calculada
        uint16_t valorADC;
        uint16_t txphase=0; // valor transmitido via ethernet
        uint16_t phaseMemory= 174; // angulo de prueba
        bool checkphase=false;


        valorADC=readADC();
        phase=calculatePhase(valorADC);
        checkphase = errorPhase(phaseMemory,phase); // true informa que si hay error

    TEST_ASSERT_EQUAL_HEX16(true,checkphase);
}
