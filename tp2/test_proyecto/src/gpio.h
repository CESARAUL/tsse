
#include <stdlib.h>  
#include<stdbool.h>
 
void gpio_default(uint16_t * puerto);
uint16_t gpio_setAngle(uint16_t angle);
bool gpio_read(void);
bool gpio_compareInOut(uint16_t angle);
void gpio_ManualPhase(void);

static uint16_t Angulo;
static uint16_t lecturaADC;

static uint16_t * gpios; // para poder capturar y mover valores de función a función
static uint16_t gpio_out;