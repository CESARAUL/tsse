#include "build/temp/_test_gpio.c"
#include "phase.h"
#include "gpio.h"
#include "unity.h"


void setUp(void)

    {







    }

void tearDown(void)

    {



    }

void test_gpioDefaultAngle(void)



    {

       uint16_t gpioValue = 0xFFFF;



        gpio_default(&gpioValue);



        UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((0xFFF7)), (UNITY_INT)(UNITY_INT16)((gpioValue)), (((void *)0)), (UNITY_UINT)(22), UNITY_DISPLAY_STYLE_HEX16);





    }







void test_txRealPhase(void)



    {

        uint16_t phase= 0;

        uint16_t valorADC;

        uint16_t txphase=0;



        valorADC=readADC();

        phase=calculatePhase(valorADC);

        txphase=txPhase(phase);

        UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((phase)), (UNITY_INT)(UNITY_INT16)((txphase)), (((void *)0)), (UNITY_UINT)(39), UNITY_DISPLAY_STYLE_HEX16);

    }



void test_okCircuits(void)



    {

        uint16_t gpioValue = 1;



        _Bool checkGpio=0;

        uint16_t angle=359;



       gpioValue = gpio_setAngle(angle);



       checkGpio=gpio_compareInOut(angle);



        UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((1)), (UNITY_INT)(UNITY_INT16)((checkGpio)), (((void *)0)), (UNITY_UINT)(54), UNITY_DISPLAY_STYLE_HEX16);

    }



void test_failCircuits(void)



    {

        uint16_t gpioValue = 1;



        _Bool checkGpio=1;

        uint16_t angle=357;



       gpioValue = gpio_setAngle(angle);



       checkGpio=gpio_compareInOut(angle);



        UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((0)), (UNITY_INT)(UNITY_INT16)((checkGpio)), (((void *)0)), (UNITY_UINT)(69), UNITY_DISPLAY_STYLE_HEX16);

    }

void test_failAngle(void)



{

        uint16_t phase= 0;

        uint16_t valorADC;

        uint16_t txphase=0;

        uint16_t phaseMemory= 174;

        _Bool checkphase=0;





        valorADC=readADC();

        phase=calculatePhase(valorADC);

        checkphase = errorPhase(phaseMemory,phase);



    UnityAssertEqualNumber((UNITY_INT)(UNITY_INT16)((1)), (UNITY_INT)(UNITY_INT16)((checkphase)), (((void *)0)), (UNITY_UINT)(85), UNITY_DISPLAY_STYLE_HEX16);

}
