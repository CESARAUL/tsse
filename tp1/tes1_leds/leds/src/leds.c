#include "leds.h"


#define ALL_OFF     0 // Constante para apgar todos los leds
#define ALL_ON      1

#define BIT_ON      1 //constante para fijar en 1 el primer bit
#define BIT_OFFSET  1 //Constante para convertir el numero de led en bit 

static uint16_t * leds;

uint16_t Leds_Bits(uint16_t led){ // asignar 1 a la posición del led

    return (BIT_ON << (led - BIT_OFFSET));
}

void Leds_Create(uint16_t * puerto){
    leds = puerto;
    *puerto = ALL_OFF;
     
}

void Led_On(uint8_t led){

    *leds |= Leds_Bits(led); // OR para asignar el valor  nuevo a leds
}

void Led_Off(uint16_t led)
{

    *leds &= ~Leds_Bits(led);// AND con la negación 1101 0010 ejemp

 
}
void Leds_TurnAllOn(void) {
    *leds = ALL_ON;
      
}
void Leds_TurnAllOff(void) {
    *leds = ALL_OFF;
      
}
bool Leds_IsOn(uint8_t n) {
    return *leds & (BIT_ON << (n - 1));
}