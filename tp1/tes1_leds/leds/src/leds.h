#include <stdint.h>
#include <stdbool.h>

void Leds_Create(uint16_t * puerto); // paso 2

void Led_On(uint8_t led);

void Led_Off(uint16_t led);
/*
void Leds_On_All();

void Leds_Off_All();

bool Leds_IsOn(int16_t led);
*/