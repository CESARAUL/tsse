/*DespuésdelainicializacióntodoslosLEDs deben quedar apagados.
• SepuedeprenderunLEDindividual.
• SepuedeapagarunLEDindividual.
• SepuedenprenderyapagarmúltiplesLED’s.
• SepuedenprendertodoslosLEDsdeunavez.
• SepuedenapagartodoslosLEDsdeunavez.
• SepuedeconsultarelestadodeunLED.
• Revisarlimitesdelosparametros.
• Revisarparámetrosfueradeloslimites.*/

#include "unity.h"
#include "leds.h" //

#define ALL_OFF     0 // Constante para apgar todos los leds
#define ALL_ON      1

#define BIT_ON      1 //constante para fijar en 1 el primer bit
#define BIT_OFF     0

#define BIT_OFFSET  1 //Constante para convertir el numero de led en bit 



static uint16_t ledsVirtuales;// inicializamos en 0xFFFF

void setUp(void)
{
   Leds_Create(&ledsVirtuales); // en cada funcion debemos llamar a esta funcion es por eso que la ponemos en setup
}
void tearDown(void)
{

}

void test_ledsOffAfterCreate(void)//==DespuésdelainicializacióntodoslosLEDs deben quedar apagados.
{  
   
   uint16_t ledsVirtuales=ALL_ON;// conservar variable y
   Leds_Create(&ledsVirtuales); //  funcion ayuda a interpretar el test 
   TEST_ASSERT_EQUAL(BIT_OFF, ledsVirtuales);// la variable debe terminar en 0
}

 void test_Led_On(void)//==• SepuedeprenderunLEDindividual.

{   
    Leds_Create(&ledsVirtuales); // en cada funcion debemos llamar a las funciones que se encesita
    const uint8_t led = 3; // numero de led a encender 
    Led_On(led);
    TEST_ASSERT_EQUAL(BIT_ON << (led-BIT_OFFSET), ledsVirtuales); 
}


 void test_Led_Off(void)//==• SepuedeapagarunLEDindividual.
{  
    const uint8_t  led = 3;
    Leds_Create(&ledsVirtuales);
    Led_On(led); // para apagarlo debo prenderlo primero
    Led_Off(led);
    TEST_ASSERT_EQUAL(BIT_OFF, ledsVirtuales);
}

//==• SepuedenprenderyapagarmúltiplesLED’s.
 void test_Multiple_Led_OnOff(void)
 {
     Led_On(2);
     Led_On(5);
     Led_Off(2);
     TEST_ASSERT_EQUAL(BIT_ON<<(5-BIT_OFFSET),ledsVirtuales);
 }
void test_TurnAllOn(void) {
   Leds_TurnAllOn();
   TEST_ASSERT_EQUAL(ALL_ON, ledsVirtuales);   
}
void test_TurnAllOff(void) {
   Leds_TurnAllOff();
   TEST_ASSERT_EQUAL(ALL_OFF, ledsVirtuales);   
}

void test_IsOn() {
const int led = 2;
    TEST_ASSERT_EQUAL_INT(false, Leds_IsOn(led));
    Led_On(led);
    TEST_ASSERT_EQUAL_INT(true, Leds_IsOn(led));
}