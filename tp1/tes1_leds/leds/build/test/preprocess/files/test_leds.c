#include "build/temp/_test_leds.c"
#include "src/leds.h"
#include "/Library/Ruby/Gems/2.6.0/gems/ceedling-0.29.1/vendor/unity/src/unity.h"
static uint16_t ledsVirtuales;



void setUp(void)

{

   Leds_Create(&ledsVirtuales);

}

void tearDown(void)

{



}



void test_ledsOffAfterCreate(void)

{



   uint16_t ledsVirtuales=1;

   Leds_Create(&ledsVirtuales);

   UnityAssertEqualNumber((UNITY_INT)((0)), (UNITY_INT)((ledsVirtuales)), (((void*)0)), (UNITY_UINT)(40), UNITY_DISPLAY_STYLE_INT);

}



 void test_Led_On(void)



{

    Leds_Create(&ledsVirtuales);

    const uint8_t led = 3;

    Led_On(led);

    UnityAssertEqualNumber((UNITY_INT)((1 << (led-1))), (UNITY_INT)((ledsVirtuales)), (((void*)0)), (UNITY_UINT)(49), UNITY_DISPLAY_STYLE_INT);

}





 void test_Led_Off(void)

{

    const uint8_t led = 3;

    Leds_Create(&ledsVirtuales);

    Led_On(led);

    Led_Off(led);

    UnityAssertEqualNumber((UNITY_INT)((0)), (UNITY_INT)((ledsVirtuales)), (((void*)0)), (UNITY_UINT)(59), UNITY_DISPLAY_STYLE_INT);

}





 void test_Multiple_Led_OnOff(void)

 {

     Led_On(2);

     Led_On(5);

     Led_Off(2);

     UnityAssertEqualNumber((UNITY_INT)((1<<(5-1))), (UNITY_INT)((ledsVirtuales)), (((void*)0)), (UNITY_UINT)(68), UNITY_DISPLAY_STYLE_INT);

 }

void test_TurnAllOn(void) {

   Leds_TurnAllOn();

   UnityAssertEqualNumber((UNITY_INT)((1)), (UNITY_INT)((ledsVirtuales)), (((void*)0)), (UNITY_UINT)(72), UNITY_DISPLAY_STYLE_INT);

}

void test_TurnAllOff(void) {

   Leds_TurnAllOff();

   UnityAssertEqualNumber((UNITY_INT)((0)), (UNITY_INT)((ledsVirtuales)), (((void*)0)), (UNITY_UINT)(76), UNITY_DISPLAY_STYLE_INT);

}



void test_IsOn() {

const int led = 2;

    UnityAssertEqualNumber((UNITY_INT)((0)), (UNITY_INT)((Leds_IsOn(led))), (((void*)0)), (UNITY_UINT)(81), UNITY_DISPLAY_STYLE_INT);

    Led_On(led);

    UnityAssertEqualNumber((UNITY_INT)((1)), (UNITY_INT)((Leds_IsOn(led))), (((void*)0)), (UNITY_UINT)(83), UNITY_DISPLAY_STYLE_INT);

}
